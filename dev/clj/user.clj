(ns clj.user
  (:require [cheshire.core]))

(def facts
  (cheshire.core/decode (slurp "/home/runejuhl/projects/puppet-census/test/resources/facts01.json")))

;; (count facts)

;; (->> facts
;;      (partition-by #(get % "certname"))
;;      (map #(select-keys % #{})))

(defn facts->map
  [facts]
  (->> facts
       (map (fn [{:strs [name value] :as fact}]
              {name value}))
       (reduce conj)))

;; (->> facts
;;      (group-by #(get % "certname"))
;;      (map #(update % 1 facts->map))
;;      (into {}))


(def response
  (let [path           "/pdb/query/v4/reports"
        request-method :get
        headers        {}
        query-string
        {:query    (cheshire.core/encode ["=", "certname", "kmsload118.kmsext.dk"])
         :order_by (cheshire.core/encode [{:field :certname
                                           :order :desc}])
         :limit    2}
        form-params    nil
        ;; {:query    ["=", "certname", "kmsload118.kmsext.dk"]
        ;;  :order_by [{"field" "certname"
        ;;              "order" "desc"}]
        ;;  :limit    2}
        body           nil]
    (clj-http.client/request
     {:url          (str "http://localhost:62880/" path)
      :method       request-method
      :content-type :json
      :headers      (dissoc headers "content-length")
      :query-params query-string
      :body         (if body (cheshire.core/encode body))})))

(def reports
  (cheshire.core/decode (slurp "/tmp/reports.json")))

(count (->> response
            :body
            (cheshire.core/decode)))
(spit "/tmp/response.json" (:body response))

(count reports)
(first reports)
;; => {"receive_time" "2020-08-14T00:06:22.526Z",
;;     "corrective_change" nil,
;;     "code_id" nil,
;;     "noop" false,
;;     "certname" "kmsload118.kmsext.dk",
;;     "puppet_version" "6.17.0",
;;     "hash" "a4de52f8d796a4b127db09c1c58454d13ee83f2b",
;;     "cached_catalog_status" "not_used",
;;     "transaction_uuid" "269d61be-e49b-4221-8b54-16301aeb9541",
;;     "configuration_version" "1597363565",
;;     "status" "unchanged",
;;     "job_id" nil,
;;     "producer" "puppet",
;;     "catalog_uuid" "1b489299-1506-4bc5-aa99-e3a4996fb252",
;;     "logs"
;;     {"data"
;;      [{"file" nil,
;;        "line" nil,
;;        "tags" ["notice"],
;;        "time" "2020-08-14T02:06:22.400+02:00",
;;        "level" "notice",
;;        "source" "Puppet",
;;        "message" "Applied catalog in 12.60 seconds"}],
;;      "href"
;;      "/pdb/query/v4/reports/a4de52f8d796a4b127db09c1c58454d13ee83f2b/logs"},
;;     "noop_pending" false,
;;     "end_time" "2020-08-14T00:06:22.400Z",
;;     "type" "agent",
;;     "environment" "production",
;;     "start_time" "2020-08-14T00:05:59.146Z",
;;     "resource_events"
;;     {"data" nil,
;;      "href"
;;      "/pdb/query/v4/reports/a4de52f8d796a4b127db09c1c58454d13ee83f2b/events"},
;;     "metrics"
;;     {"data"
;;      [{"name" "changed", "value" 0, "category" "resources"}
;;       {"name" "corrective_change", "value" 0, "category" "resources"}
;;       {"name" "failed", "value" 0, "category" "resources"}
;;       {"name" "failed_to_restart", "value" 0, "category" "resources"}
;;       {"name" "out_of_sync", "value" 0, "category" "resources"}
;;       {"name" "restarted", "value" 0, "category" "resources"}
;;       {"name" "scheduled", "value" 0, "category" "resources"}
;;       {"name" "skipped", "value" 0, "category" "resources"}
;;       {"name" "total", "value" 544, "category" "resources"}
;;       {"name" "anchor", "value" 4.5188299999999993E-4, "category" "time"}
;;       {"name" "augeas", "value" 0.270752891, "category" "time"}
;;       {"name" "catalog_application",
;;        "value" 12.596261522732675,
;;        "category" "time"}
;;       {"name" "concat_file", "value" 2.90898E-4, "category" "time"}
;;       {"name" "concat_fragment", "value" 1.33911E-4, "category" "time"}
;;       {"name" "config_retrieval", "value" 3.1291108056902885, "category" "time"}
;;       {"name" "convert_catalog", "value" 1.204510536044836, "category" "time"}
;;       {"name" "cron", "value" 0.007080431999999999, "category" "time"}
;;       {"name" "exec", "value" 0.057316004000000004, "category" "time"}
;;       {"name" "fact_generation", "value" 4.871630712412298, "category" "time"}
;;       {"name" "file", "value" 0.657798032, "category" "time"}
;;       {"name" "file_line", "value" 0.001468838, "category" "time"}
;;       {"name" "filebucket", "value" 4.7687E-5, "category" "time"}
;;       {"name" "firewall", "value" 0.0030603839999999998, "category" "time"}
;;       {"name" "group", "value" 0.004621867, "category" "time"}
;;       {"name" "host", "value" 0.004862527, "category" "time"}
;;       {"name" "ini_setting", "value" 9.24125E-4, "category" "time"}
;;       {"name" "mailalias", "value" 2.03935E-4, "category" "time"}
;;       {"name" "mount", "value" 0.0037285029999999998, "category" "time"}
;;       {"name" "node_retrieval", "value" 0.21983063872903585, "category" "time"}
;;       {"name" "package", "value" 3.931538257, "category" "time"}
;;       {"name" "plugin_sync", "value" 1.0426102317869663, "category" "time"}
;;       {"name" "resources", "value" 1.32634E-4, "category" "time"}
;;       {"name" "schedule", "value" 2.64147E-4, "category" "time"}
;;       {"name" "service", "value" 0.38645673399999997, "category" "time"}
;;       {"name" "ssh_authorized_key", "value" 0.001394657, "category" "time"}
;;       {"name" "sshkey", "value" 0.03754572499999998, "category" "time"}
;;       {"name" "total", "value" 23.25388261, "category" "time"}
;;       {"name" "transaction_evaluation",
;;        "value" 12.520211725495756,
;;        "category" "time"}
;;       {"name" "user", "value" 0.423885349, "category" "time"}
;;       {"name" "yumrepo", "value" 0.001770055, "category" "time"}
;;       {"name" "total", "value" 0, "category" "changes"}
;;       {"name" "failure", "value" 0, "category" "events"}
;;       {"name" "success", "value" 0, "category" "events"}
;;       {"name" "total", "value" 0, "category" "events"}],
;;      "href"
;;      "/pdb/query/v4/reports/a4de52f8d796a4b127db09c1c58454d13ee83f2b/metrics"},
;;     "producer_timestamp" "2020-08-14T00:06:22.509Z",
;;     "report_format" 10}
