#!/usr/bin/env sh

set -eu

base_name="${CI_REGISTRY_IMAGE}/${CI_PROJECT_NAME}"

# If namespace is "obmondo" we tag it with the git tag or `latest`, otherwise we
# tag it with username and branch
if [ "${CI_PROJECT_NAMESPACE}" = 'runejuhl' ]; then
  tag="${CI_COMMIT_TAG:-latest}"
else
  tag="${CI_PROJECT_NAMESPACE}-${CI_COMMIT_BRANCH}"
fi

# Write base Gitlab CI information for Kaniko;
# https://docs.gitlab.com/ee/ci/docker/using_kaniko.html#building-a-docker-image-with-kaniko
cat > /kaniko/.docker/config.json <<EOF
{
  "auths": {
    "${CI_REGISTRY}": {
      "username": "${CI_REGISTRY_USER}",
      "password": "${CI_REGISTRY_PASSWORD}"
    }
  }
}
EOF

/kaniko/executor                                        \
  --context .                              \
  --dockerfile ./Dockerfile                \
  --destination "${base_name}:${tag}" \
  --destination "${base_name}:$(date +%s)"
