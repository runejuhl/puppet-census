(ns puppet-census.views
  (:require
   [taoensso.timbre :as log]
   [re-frame.core :as re-frame]

   [puppet-census.time :refer [humanize-ts]]
   [puppet-census.util :refer [pretty-print seqf]]
   [puppet-census.subs :as subs]
   [puppet-census.puppetdb :as puppetdb]
   [puppet-census.time :as time]
   [puppet-census.components :refer [table]]
   [puppet-census.routes :as routes :refer [link current-page]]))

(defn meta-update-box
  [obj]
  (if-let [{:keys [timestamp] :as metadata} (meta obj)]
    (let [time @(re-frame/subscribe [::subs/time])]
      [:div.meta
       [:div.last-updated
        (if timestamp
          (str "Last updated "
               (time/human timestamp))
          (str "Never updated"))]])))

(defn refresh-nodes []
  [:div.refresh-buttons
   [:button
    {:on-click #(do
                  (re-frame/dispatch (puppetdb/nodes))
                  (re-frame/dispatch (puppetdb/facts)))}
    "Refresh nodes"]
   [:button
    {:on-click #(re-frame/dispatch (puppetdb/notes))}
    "Refresh notes"]])

(def filter-production-only ^{:doc "Production environment only"}
  (fn [row]
    (= (:report_environment row) "production")))

(def filter-failed-only ^{:doc "Failed only"}
  (fn [row]
    (= (:latest_report_status row) "failed")))

(defn servers-page []
  (let [servers (vals @(re-frame/subscribe [::subs/servers]))]
    [:div
     [:h1 "Servers"
      [refresh-nodes]]
     [table {:id           :servers-panel-table
             :show-header? true
             :columns      [{:path  [:certname]
                             :title "Certname"
                             :formatter
                             (fn [v]
                               [:a
                                {:href (link ::routes/server {:certname v})}
                                v])}
                            {:path  [:facts "ipaddress"]
                             :title "IP"}
                            {:path  [:facts "osfamily"]
                             :title "OS"}
                            {:path  [:facts "operatingsystemrelease"]
                             :title "Release"}
                            {:path  [:facts "kernelversion"]
                             :title "Kernel"}
                            {:title "CPU"
                             :row-fn
                             (fn [row]
                               (let [physical (get-in row [:facts "physicalprocessorcount"])
                                     cores (get-in row [:facts "processorcount"])]
                                 (str physical "x (" cores " core" (if (> cores 1) "s") ") " (get-in row [:facts "processors" :models 0]))))}
                            {:path  [:facts "memorysize"]
                             :title "RAM"}
                            {:path  [:facts "reboot_required"]
                             :title "Reboot?"
                             :formatter
                             (fn [v]
                               [:div {:class (seqf [(if v
                                                      :red)])}
                                (if v
                                  "yes"
                                  "no")])}

                            {:path  [:facts "virtual"]
                             :title "Virtual"}
                            {:path  [:report_environment]
                             :title "Environment"}
                            {:path  [:latest_report_status]
                             :title "Report status"}
                            {:path  [:comment]
                             :title "Comment"
                             :row-fn
                             (fn [{:keys [certname comment] :as row}]
                               [:form
                                [:textarea.comment
                                 {:on-key-down
                                  (fn [event]
                                    ;; submit textarea contents on ctrl+enter
                                    (let [ctrl?  (.-ctrlKey event)
                                          ;; keycode 10 is LF, 13 is CR; Windows
                                          ;; and macOS
                                          enter? (#{10 13} (.-keyCode event))]
                                      (when (and ctrl? enter?)
                                        (log/debug event  ctrl? enter?)
                                        (.preventDefault event)
                                        (let [value (-> event
                                                        (.-target)
                                                        (.-value))]
                                          (log/debug :new-note certname event value)
                                          (re-frame/dispatch
                                           (puppetdb/set-note! certname
                                                               value))))))
                                  :default-value comment
                                  :placeholder "Click here to add a server note. Hit Ctrl+Enter to save."}]])}]
             :sorters    #{}
             :filter-fns #{filter-production-only filter-failed-only}
             :filters    #{}
             :selected   #{}
             :rows       servers
             :row-metadata-fn
             (fn [row-data]
               {:class [(get-in row-data [:latest_report_status])]})}]]))

(defn tag-button
  [& children]
  `[:span.button.tag
    ~@children])

(defn report-tag-button
  [row metric]
  [tag-button
   {:class [metric]
    :title (name metric)}
   (get-in row [:metrics :data :resources metric :value] 0)])

(defn reports
  [{:keys [certname facts] :as server}]
  (re-frame/dispatch (puppetdb/reports {:certname certname}))
  (let [all-reports (vals @(re-frame/subscribe [::subs/reports]))
        reports     (filter #(= (:certname %) certname) all-reports)]
    [:section.reports
     [:h2 "Reports"]

     [table {:id            (keyword (str "reports-table-" certname))
             :columns       [{:path  [:end_time]
                              :title "End time"
                              :row-fn
                              (fn [row]
                                [:a
                                 {:href (link ::routes/report {:hash (:hash row)})}
                                 (humanize-ts (:end_time row))])}
                             {:key   :report-resources
                              :title "Resources"
                              :row-fn
                              (fn [row]
                                [:div
                                 (map #(report-tag-button row %)
                                      [:changed
                                       :failed
                                       :success
                                       :skipped
                                       :total])])}]
             :rows          reports
             :sort-reverse? true
             :page-size     50}]]))

(defn pre
  [content]
  [:pre content])

(defn report-page
  [route]
  (let [hash    (-> route
                    :parameters
                    :path
                    :hash)
        reports @(re-frame/subscribe [::subs/reports])
        report  (get reports hash)]
    (when-not report
      (re-frame/dispatch (puppetdb/report hash)))
    (when-let [{:keys [certname end_time environment logs puppet_version
                       resource_events status]} report]
      [:section.report
       [:h2 "Report from " certname " " (humanize-ts end_time)
        [:span.state
         {:class status
          :title (str "Last report status: " status)}]]
       [:section.events
        [:h3 "Events"]
        [table {:id        (keyword (str "report-table-events-" hash))
                :columns   [{:title "Resource"
                             :row-fn
                             (fn [{:keys [resource_type resource_title] :as row}]
                               [pre
                                (str resource_type "[" resource_title "]")])
                             :path  [:resource]}
                            {:title "Status"
                             :path  [:status]}]
                :rows      (:data resource_events)
                :page-size 50}]]
       [:section.logs
        [:h3 "Logs"]
        ;; FIXME: this table seems to be missing unique keys for react
        [table {:id        (keyword (str "report-table-logs-" hash))
                :columns   [{:title     "Message"
                             :path      [:message]
                             :formatter pre}
                            {:title     "Source"
                             :path      [:source]
                             :formatter pre}
                            {:title     "Timestamp"
                             :path      [:time]
                             :formatter humanize-ts}
                            {:title "Location"
                             :row-fn
                             (fn [{:keys [file line] :as row}]
                               (when file
                                 [:pre
                                  (str (-> file
                                           (clojure.string/split
                                            (re-pattern (str "/" environment "/")) 2)
                                           (second)) ":" line)]))}]
                :row-metadata-fn
                (fn [row-data]
                  {:class [:log (get-in row-data [:level])]})
                :page-size 50
                :rows      (:data logs)}]]])))

(defn facts
  [{:keys [certname facts facts_environment facts_timestamp] :as server}]
  (when facts
    [:section.facts
     [:h2 "Facts"
      [:span.meta "Environment " facts_environment " " (humanize-ts facts_timestamp)]]
     [table {:id        (keyword (str "facts-table-" certname))
             :columns   [{:path      [0]
                          :title     "Fact"
                          :formatter (fn [v]
                                       [:a
                                        {:href (link ::routes/fact {:fact v})}
                                        v])}
                         {:path      [1]
                          :title     "Value"
                          :formatter pretty-print}]
             :rows      facts
             :page-size 50}]]))

(defn hardware-specs-block
  [icon type value]
  [:div.spec
   [:img
    {:src (str "/icons/" (name icon) ".svg")}]
   [:div value]])

(defn server-details
  [server]
  [:section.server-details
   [:h2 "Details"]
   [:div.specs
    (let [cpu-models (get-in server [:facts "processors" :models])
          cpu-count  (count cpu-models)
          cpu        (first cpu-models)
          memory     (get-in server [:facts "memory" :system :total])
          virtual    (get-in server [:facts "virtual"])]
      [:div.wrapper
       (hardware-specs-block :cpu "CPU" (str cpu-count " core " cpu))
       (hardware-specs-block :ram "RAM" (str memory " RAM"))
       (hardware-specs-block (case virtual
                               "physical" :server-rack
                               nil        :uncertainty
                               :sun-cloud) "Type" virtual)])]])

(defn server-page [route]
  (let [servers                        @(re-frame/subscribe [::subs/servers])
        certname                       (-> route
                                           :parameters
                                           :path
                                           :certname)
        server                         (get servers certname)
        {:keys [latest_report_status]} server]
    [:section.server
     [:h1 "Server " certname
      [:span.state
       {:class latest_report_status
        :title (str "Last report status: " latest_report_status)}]]
     [server-details server]
     [reports server]
     [facts server]]))

(defn fact-page [route]
  (let [servers @(re-frame/subscribe [::subs/servers])
        fact    (-> route
                    :parameters
                    :path
                    :fact)
        facts   (->> servers
                     (map (fn [[k v]]
                            [k (get-in v [:facts fact])]))
                     (filter (fn [[_ v]]
                               v)))]
    [:div.fact
     [:h2 (str "Fact " fact)]
     [table {:id        (hash [:table :fact fact])
             :columns   [{:path  [0]
                          :title "Server"
                          :formatter (fn [v]
                                       [:a
                                        {:href (link ::routes/server {:certname v})}
                                        v])}
                         {:path      [1]
                          :title     "Value"
                          :formatter pretty-print}]
             :page-size 50
             :rows      facts}]]))

(defn facts-page []
  (let [servers @(re-frame/subscribe [::subs/servers])]
    [:section.facts
     [:h1 "Facts"]
     (let [facts-count (->> servers
                            (vals)
                            (map (fn [x]
                                   (-> x
                                       :facts
                                       (keys))))
                            (apply concat)
                            (sort)
                            (partition-by identity)
                            (map (fn [x] {(first x) (count x)}))
                            (into {}))]
       [table {:id        :reports-page-table
               :columns   [{:path  [0]
                            :title "Fact"
                            :formatter
                            (fn [v]
                              [:a
                               {:href (link ::routes/fact {:fact v})}
                               v])}
                           {:path  [1]
                            :title "Count"}]
               :page-size 100
               :rows      facts-count}])]))

(defn reports-page []
  (let [reports @(re-frame/subscribe [::subs/reports])]
    [:section.reports
     [:h1 "Reports "]
     [table {:id            :reports-page-table
             :columns       [{:path  [:certname]
                              :title "Certname"
                              :formatter
                              (fn [v]
                                [:a
                                 {:href (link ::routes/server {:certname v})}
                                 v])}
                             {:path  [:end_time]
                              :title "End time"}
                             {:path  [:status]
                              :title "Status"}
                             {:path  [:certname]
                              :title "Certname"}
                             {:path  [:configuration_version]
                              :title "Configuration #"}
                             {:path  [:agent_version]
                              :title "Agent version"}]
             :sorters       #{}
             :sort-column   [:end_time]
             :sort-reverse? true
             :filter-fns    #{}
             :filters       #{}
             :selected      #{}
             :page-size     50
             :rows          (vals reports)
             :row-metadata-fn
             (fn [row-data]
               {:class [(get-in row-data [:status])]})}]]))

(defn nodes-page []
  [:div ;; re-com/v-box
   servers-page])

(defn metric-block
  ([header content]
   (metric-block nil header content))
  ([opts header content]

   (let [[first-word remaining] (clojure.string/split header #" " 2)]
     [:div.metric
      opts
      [:h2
       [:b first-word]
       (str " " remaining)]
      [:div content]])))

(defn overview-page []
  (let [servers @(re-frame/subscribe [::subs/servers])]

    [:div.overview
     (let [statuses         (->> servers
                                 (vals)
                                 (map :latest_report_status))
           count-fn         (fn [status] (count (filter #(= status %) statuses)))
           failed-count     (count-fn "failed")
           pending-count    (count-fn "noop") ;FIXME: is this correct?
           changed-count    (count-fn "changed")
           unchanged-count  (count-fn "unchanged")
           unreported-count (count-fn "unreported")]
       [:div.metrics
        [metric-block
         {:class :failed}
         (str failed-count " nodes")
         "with status failed"]

        [metric-block
         {:class :pending}
         (str pending-count " nodes")
         "with status pending"]

        [metric-block
         {:class :changed}
         (str changed-count " nodes")
         "with status changed"]

        [metric-block
         {:class :unchanged}
         (str unchanged-count " nodes")
         "with status unchanged"]

        [metric-block
         {:class :unreported}
         (str unreported-count " nodes")
         "with status unreported"]

        [metric-block
         {:class :total}
         (str (count statuses) " nodes")
         "total"]])

     [:div.divider]

     [:div.node-status-detail
      (let [problematic-servers (->> servers
                                     (vals)
                                     (filter (fn [{:keys [latest_report_status]}]
                                               (not= latest_report_status "unchanged"))))]
        [:div
         [:h1 (str "Node status detail (" (count problematic-servers) ")")]

         [table {:id          :overview-detail-table
                 :show-header? true
                 :columns     [{:path  [:latest_report_status]
                                :title "Report status"}
                               {:path  [:certname]
                                :title "Certname"
                                :formatter
                                (fn [v]
                                  [:a
                                   {:href (link ::routes/server {:certname v})}
                                   v])}

                               {:path  [:report_timestamp]
                                :title "Report"
                                :row-fn
                                (fn [{:keys [report_timestamp latest_report_hash] :as row}]
                                  [:a
                                   {:href (link ::routes/report {:hash latest_report_hash})}
                                   (humanize-ts report_timestamp)])}
                               {:path  [:cached_catalog_status]
                                :title "Cached"
                                :formatter
                                (fn [v]
                                  (if (= v "not_used")
                                    "no"
                                    "yes"))}]
                 :rows         problematic-servers
                 :sort-column  [:report_timestamp]
                 :sort-reverse? true
                 :page-size    100
                 :row-metadata-fn
                 (fn [row-data]
                   {:class [(get-in row-data [:latest_report_status])]})}]])]]))

;; main


(defn- panels [route]
  (let [panel (-> route
                  :data
                  :view)]
    (case panel
      :overview-page [overview-page]
      :fact-page     [fact-page route]
      :facts-page    [facts-page]
      :server-page   [server-page route]
      :report-page   [report-page route]
      :reports-page  [reports-page route]
      :nodes-page    [servers-page]
      [:div
       [:h1 "404, woops!"]])))

(defn show-panel [panel-name]
  [panels panel-name])

(defn throbber []
  (let [throbber? @(re-frame/subscribe [::subs/throbber?])]
    (log/debug :throbber throbber?)
    (when (pos? throbber?)
      [:div#throbber])))

(defn main-panel []
  (let [route        (re-frame/subscribe [::subs/route])
        current-page (-> @route
                         :data
                         :name)
        pages        [{:title "Overview"
                       :link  ::routes/overview}
                      {:title "Nodes"
                       :link  ::routes/nodes}
                      {:title "Facts"
                       :link  ::routes/facts}
                      {:title "Reports"
                       :link  ::routes/reports}]]
    [:div
     [:nav
      [:ol
       [:li
        [:button "Census"]]
       (for [{:keys [title link]} pages]
         [:li
          {:key   link
           :class (seqf [(if (= current-page link)
                           :current)])}
          [:a
           {:href (routes/link link)}
           [:button title]]])]]
     [:div.body
      [panels @route]
      [throbber]]]))
