(ns puppet-census.time
  (:require
   [cljs-time.format :as time.format]

   [clojure.contrib.humanize :as humanize]))

(defn format
  [ts]
  (time.format/unparse
   (time.format/formatters :basic-date-time) ts))

(defn human
  [ts]
  (humanize/datetime ts))

(defn humanize-ts
  [ts]
  (human (time.format/parse
          (time.format/formatters :date-time)
          ts)))
