(ns puppet-census.routes
  (:require
   [taoensso.timbre :as log]

   [reagent.core :as r]
   [reitit.frontend :as rf]
   [reitit.frontend.easy :as rfe]
   [reitit.coercion.spec :as rss]

   [re-frame.core :as re-frame]
   [puppet-census.events :as events]))

(defonce match (r/atom nil))

(defn home-page []
  [:div
   [:h2 "Welcome to frontend"]

   [:button
    {:type "button"
     :on-click #(rfe/push-state ::item {:id 3})}
    "Item 3"]

   [:button
    {:type "button"
     :on-click #(rfe/replace-state ::item {:id 4})}
    "Replace State Item 4"]])

(defn about-page []
  [:div
   [:h2 "About frontend"]
   [:ul
    [:li [:a {:href "http://google.com"} "external link"]]
    [:li [:a {:href (rfe/href ::foobar)} "Missing route"]]
    [:li [:a {:href (rfe/href ::item)} "Missing route params"]]]

   [:div
    {:content-editable true
     :suppressContentEditableWarning true}
    [:p "Link inside contentEditable element is ignored."]
    [:a {:href (rfe/href ::frontpage)} "Link"]]])

(defn item-page [match]
  (let [{:keys [path query]} (:parameters match)
        {:keys [id]} path]
    [:div
     [:h2 "Selected item " id]
     (if (:foo query)
       [:p "Optional foo query param: " (:foo query)])]))

(def routes
  [["/"
    {:name ::overview
     :view :overview-page}]

   ["/nodes"
    {:name ::nodes
     :view :nodes-page}]

   ["/about"
    {:name ::about
     :view about-page}]

   ["/facts"
    {:name ::facts
     :view :facts-page}]

   ["/fact/:fact"
    {:name ::fact
     :view :fact-page
     :parameters {:path {:fact string?}}}]

   ["/reports"
    {:name ::reports
     :view :reports-page}]

   ["/report/:hash"
    {:name ::report
     :view :report-page
     :parameters {:path {:hash string?}}}]

   ["/server/:certname"
    {:name ::server
     :view :server-page
     :parameters {:path {:certname string?}}}]])

(defn start!
  []
  (log/debug "starting routing")
  (rfe/start!
   (rf/router routes {:data {:coercion rss/coercion}})
   ;; on-navigate fn
   (fn
     [m]
     (log/debug :on-navigate (:view (:data m)) (:view (:data @match)))
     (re-frame/dispatch [::events/set-active-panel (:view (:data m))])
     (re-frame/dispatch [::events/set-route m])
     (reset! match m))
    ;; set to false to enable HistoryAPI
   {:use-fragment true}))

(defn link
  ([p]
   (rfe/href p))
  ([p args]
   ;; FIXME: reitit ends up throwing an exception due to asser if it is passed a
   ;; nil value in a map. Since I haven't found out what's causing this, the
   ;; following checks the arguments to ensure that the assert doesn't break the
   ;; app.
   (if (->> args
            (vals)
            (some nil?)
            (not))
     (rfe/href p args)
     (do
       (log/debug :link "got invalid link route" p args)
       nil))))

(defn current-page
  []
  @match)
