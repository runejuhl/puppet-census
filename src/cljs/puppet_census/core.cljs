(ns puppet-census.core
  (:require
   [reagent.dom :as rdom]
   [re-frame.core :as re-frame]
   [puppet-census.events :as events]
   [puppet-census.routes :as routes]
   [puppet-census.views :as views]
   [puppet-census.config :as config]
   [puppet-census.puppetdb :as puppetdb]))

(defn dev-setup []
  (when config/debug?
    (println "dev mode")))

(defn ^:dev/after-load mount-root []
  (re-frame/clear-subscription-cache!)
  (let [root-el (.getElementById js/document "app")]
    (rdom/unmount-component-at-node root-el)
    (rdom/render [views/main-panel] root-el)))

(defn fetch-data!
  []
  (re-frame/dispatch (puppetdb/nodes))
  (re-frame/dispatch (puppetdb/notes))
  (re-frame/dispatch (puppetdb/facts))
  (re-frame/dispatch (puppetdb/reports)))

(defn init []
  (routes/start!)
  (re-frame/dispatch-sync [::events/initialize-db])
  (fetch-data!)
  (dev-setup)
  (mount-root))
