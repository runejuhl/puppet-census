(ns puppet-census.config)

(def debug?
  ^boolean goog.DEBUG)

(def puppetdb-base
  (if debug?
    "http://localhost:3000"))
