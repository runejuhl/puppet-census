(ns puppet-census.events
  (:require
   [puppet-census.util :refer [has? to-map]]
   [puppet-census.config :refer [puppetdb-base]]

   [re-frame.core :as re-frame]
   [taoensso.timbre :as log]

   [puppet-census.db :as db]
   [ajax.core :as ajax]
   [day8.re-frame.http-fx]
   [cljs-time.core]))

(re-frame/reg-event-db
 ::initialize-db
 (fn [_ _]
   db/default-db))

(defn dispatch-timer-event
  []
  (let [now (js/Date.)]
    (re-frame/dispatch [:timer now])))

(defonce do-timer
  ;; dispatch every 10 seconds
  (js/setInterval dispatch-timer-event 10000))

(re-frame/reg-event-db
 :timer
 (fn [db [_ new-time]] ;; note how the 2nd parameter is destructured to obtain
                      ;; the data value
   (assoc db :time new-time)))

(re-frame/reg-event-db
 ::set-active-panel
 (fn [db [_ active-panel]]
   (assoc db :active-panel active-panel)))

(re-frame/reg-event-db
 ::set-route
 (fn [db [_ route]]
   (assoc db :route route)))

(defn url-for
  [endpoint]
  (->> endpoint
       (map name)
       (clojure.string/join "/")
       (str puppetdb-base "/puppetdb/pdb/query/v4/")))

(re-frame/reg-event-fx
 :fetch
 (fn [{:keys [db]}
      [_ {:keys
          [method post-success-fn map-key force endpoint db-path path params
           uri query-string query deep-merge?
           on-success] :as args}]]
   ;; we do nothing if we already have a reasonably recent cached value, unless
   ;; `force` is non-nil
   (let [timestamp       (some-> db
                                 (get-in path)
                                 (meta)
                                 :timestamp)
         post-success-fn (or post-success-fn
                             (if map-key
                               (to-map map-key)))]
     (log/info :params args timestamp)
     {:db         (update db :throbber inc)
      :http-xhrio {:method          (or method :get)
                   :uri             (or uri (url-for path))
                   :params          params
                   :query           (if query (.stringify js/JSON (clj->js query)))
                   :timeout         5000
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [(or on-success :good-http-result)
                                     {:path    path
                                      :db-path (or db-path path)
                                      :f       post-success-fn
                                      :deep-merge? deep-merge?}]
                   :on-failure      [:bad-http-result]}})))

(re-frame/reg-event-db
 :good-http-result
 (fn [db [_ {:keys [db-path path f deep-merge?]} result]]
   (let [processed-result                     (cond-> result
                                                f (f result))]
     (log/trace :good-http-result db-path path (count result) (count processed-result) deep-merge?)
     (-> db
         (update :throbber dec)
         (update-in db-path
                    (if deep-merge?
                      (partial merge-with merge)
                      merge)
                    (with-meta
                      processed-result
                      {:timestamp (cljs-time.core/now)}))))))

(re-frame/reg-event-db
 :bad-http-result
 (fn [db [what result]]
   (log/spy :debug [:bad-http-result what result])
   (update db :throbber dec)))

(re-frame/reg-event-db
 :set-component
 (fn [db [_ [chash field value]]]
   (log/spy :debug [:set-component chash field value])
   (assoc-in db [:components chash field] value)))

(re-frame/reg-event-db
 :component-update-set
 (fn [db [_ [chash field value]]]
   (let [component [:components chash field]
         has-value? (has? (get-in db component) value)
         f #((if has-value? clojure.set/difference clojure.set/union) % #{value})]
     (update-in db component f))))

(re-frame/reg-event-db
 :component-update
 (fn [db [_ [chash field f]]]
   (update-in db [:components chash field] f)))
