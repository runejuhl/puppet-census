(ns puppet-census.subs
  (:require
   [re-frame.core :as re-frame]
   [taoensso.timbre :as log]))

(re-frame/reg-sub
 ::name
 (fn [db]
   (:name db)))

(re-frame/reg-sub
 ::active-panel
 (fn [db _]
   (:active-panel db)))

(re-frame/reg-sub
 ::route
 (fn [db _]
   (:route db)))

(re-frame/reg-sub
 ::throbber?
 (fn [db _]
   (:throbber db)))

(re-frame/reg-sub
 ::facts
 (fn [db _]
   (:facts db)))

(re-frame/reg-sub
 ::nodes
 (fn [db _]
   (:nodes db)))

(re-frame/reg-sub
 ::reports
 (fn [db _]
   (:reports db)))

(re-frame/reg-sub
 ::servers
 (fn [db _]
   (:servers db)))

(re-frame/reg-sub
 ::time
 (fn [db _]     ;; db is current app state. 2nd unused param is query vector
   (:time db)))

(re-frame/reg-sub
 ::node
 (fn [db _]
   (when-let [certname (:certname db)]
     (some-> db
             :nodes
             (get certname)))))

(re-frame/reg-sub-raw
 :get-component
 (fn [db _ [chash]]
   ;; FIXME
   ;; (reaction (or (get (:components db) chash) {}))
   (get (:components db) chash)))

(re-frame/reg-sub
 :get-components
 (fn [db _]
   (:components db)))
