(ns puppet-census.puppetdb
  (:require [taoensso.timbre :as log]
            [puppet-census.util :refer [to-json to-map to-map-kv]]))

(defprotocol QueryFilter
  "Query PuppetDB using a filter, selecting the comparison function depending on
  type of `q`."
  (qfilter [q]))

(extend-protocol QueryFilter
  nil
  (qfilter [_]
    nil)

  boolean
  (qfilter [q]
    ["=" q])

  string
  (qfilter [q]
    ["=" q])

  ;; Pattern
  ;; (qfilter [q]
  ;;   ["~" q])
)

(defn ffilter
  "Create a PuppetDB query filter on a particular field."
  [field q]
  (let [[pred value] (qfilter q)]
    (if value
      [pred (name field) value])))

(defn nodes
  []
  [:fetch {:path        [:nodes]
           :map-key     [:certname]
           :db-path     [:servers]
           :deep-merge? true}])

(defn facts->map
  [facts]
  {:facts (->> facts
               (map (fn [{:keys [name value] :as fact}]
                      {name value}))
               (reduce conj))})

(defn facts
  []
  [:fetch
   {:path            [:facts]
    :db-path         [:servers]
    :deep-merge?     true
    :post-success-fn (fn [facts]
                       (log/debug [:puppetdb :facts] (first facts))
                       (->> facts
                            (group-by :certname)
                            (map #(update % 1 facts->map))
                            (into {})))}])

(defn reports
  ([]
   (reports {}))
  ([{:keys [certname limit]}]
   (let [query (if certname
                 {:query (to-json ["=", "certname", certname])})]
     [:fetch
      {:path   [:reports]
       :params (merge
                {:order_by (to-json [{:field :configuration_version
                                      :order :desc}])
                 :limit    (or limit 10)}
                query)
       :method :get
       :post-success-fn
       (fn [reports]
         ;; convert a list of maps into a nested map structure, partitioned by
         ;; category
         ((to-map [:hash])
          (map
           #(update-in %
                       [:metrics
                        :data]
                       (fn [data]
                         (->> data
                              (partition-by :category)
                              (map (fn [xs]
                                     (when (seq xs)
                                       {(->> xs
                                             (first)
                                             :category
                                             (keyword))
                                        ((to-map-kv [:name]) xs)})))
                              (into {}))))
           reports)))}])))

(defn report
  [hash]
  [:fetch
   {:path    [:reports]
    :params  {:query (to-json (ffilter :hash hash))}
    :method  :get
    :map-key [:hash]}])

(defn notes
  []
  [:fetch
   {:uri             "/notes"
    :path            [:notes]
    :method          :get
    :db-path         [:servers]
    :post-success-fn (fn [notes]
                       (log/debug [:puppetdb :notes] (first notes))
                       (into {} (map (fn [[k v]] {(name k) v}) notes)))}])

(defn set-note!
  [certname note]
  [:fetch
   {:uri             (str "/notes/" certname)
    :path            [:notes]
    :method          :post
    :params          {:comment note}
    :db-path         [:servers]
    :deep-merge?     true
    :post-success-fn (fn [notes]
                       (log/spy :debug
                                (->> notes
                                     (map (fn [[k v]] [(name k) v]))
                                     (into {}))))}])

(comment
  (qfilter)

  (map (fn [{:keys [certname] :as server}]
         (assoc server :facts (get facts certname)))
       servers))
