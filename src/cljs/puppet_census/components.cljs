(ns puppet-census.components
  (:require [re-frame.core :as re-frame :refer [dispatch subscribe]]
            [puppet-census.util :refer [has? seqf]]
            [taoensso.timbre :as log]))

(defn apply-fns [f]
  (fn [filters data]
    (reduce #(f %2 %1) data filters)))

(def apply-filters
  (apply-fns filter))

(def apply-sorters
  (apply-fns sort))

(def table-defaults
  {:filters      #{}
   :sorters      #{}
   :selected     #{}
   :page-size    5
   :page-offset  0
   :show-filters? true
   :show-header?  true
   :sort-reverse? false})

(defn table-header [{:keys [id columns sort-column sort-reverse?]}]
  [:div.thead
   [:div.tr
    (for [{:keys [path title]} columns]
      [:div.td
       {:key      path
        :class    (seqf (let [is-sort-column? (= sort-column path)]
                          [(when is-sort-column?
                             :sort)
                           (when (and is-sort-column? sort-reverse?)
                             :reverse)]))
        :on-click (fn [e]
                    ;; ensure that we don't change the URL
                    (.preventDefault e)
                    (re-frame/dispatch
                     [:set-component [id :sort-column path]])
                    (when (= sort-column path)
                      (re-frame/dispatch
                       [:set-component [id :sort-reverse?
                                        (not sort-reverse?)]])))}
       (or title path)])]])

(defn table-rows [{:keys [columns row-metadata-fn] :as opts} rows selectfn]
  [:div.tbody
   (for [row rows]
     [:div.tr
      (merge-with concat {:key      (hash row)
                          :class    (filter identity
                                            [(when (:selected (meta row)) "selected")])
                          :on-click #(selectfn row)}
                  (if row-metadata-fn
                    (row-metadata-fn row)))
      (for [{:keys [key path formatter row-fn value]} columns]
        [:div.td
         {:key (or key (hash path))}
         (cond
           row-fn
           (row-fn row)

           path
           (let [value (get-in row path)]
             (if formatter
               (formatter value)
               (str value)))

           value (str value))])])])

(defn table-filter [filterfn on-click selected]
  [:label
   [:input
    {:type :checkbox
     :on-change (on-click filterfn)
     :selected  selected}]
   (:doc (meta filterfn))])

(defn table-filters [filter-fns this]
  (let [on-click        #(fn [] (dispatch [:component-update-set [this :filters %]]))
        cs              (subscribe [:get-components])
        enabled-filters (get-in @cs [this :filters])]
    [:div.filter
     [:h4 "Filters"]
     (for [filterfn filter-fns]
       [:div
        {:key (hash filterfn)}
        (table-filter filterfn on-click (has? enabled-filters filterfn))])]))

(defn pagination-sequence
  "Create sequence of integers and add ellipsis if `offset` is not near to `0` or
  `count`."
  [offset count]
  (if (> 10 count)
    (range 0 count)
    (let [head (range 0 3)
          tail (range (- count 4) count)]
      (->> [head
            (when-not (or ((set head) offset)
                          ((set tail) offset))
              (range (- offset 3)
                     (+ offset 3)))
            tail]
           (apply concat)
           (reduce (fn [acc p]
                     (let [last-page (last acc)]
                       (concat acc
                               (seqf
                                [(when (and last-page
                                            (> (- last-page p) 1))
                                   "…")
                                 p])))) [])))))

(defn table-pagination
  [{:keys [id rowcount rowcount-processed
           offset size]}]
  (let [cs           (subscribe [:get-components])
        current-page (or (get-in @cs [id :page-offset]) 0)
        pagecount    (Math/ceil (/ rowcount-processed size))
        clickfn      #(fn [e]
                        ;; ensure that we don't change the URL
                        (.preventDefault e)
                        (dispatch [:set-component [id :page-offset %]]))
        offset       (or offset 0)]
    [:div
     [:div (str "Page " (+ offset 1) " of " pagecount
                (if (->> rowcount (- rowcount-processed) (> 0))
                  (str " (" (- rowcount rowcount-processed) " filtered)")))]
     [:div.pages
      (if (< 0 offset)
        [:a {:key      :prev
             :href     ""
             :on-click (clickfn (dec offset))}
         "←"])
      (for [offset (pagination-sequence offset pagecount)]
        (if (int? offset)
          [:a {:key      offset
               :class    (if (= current-page offset) "current")
               :href     ""
               :on-click (clickfn offset)}
           (inc offset)]
          [:span offset]))
      (if (< offset pagecount)
        [:a {:key      :next
             :href     ""
             :on-click (clickfn (inc offset))}
         "→"])]]))

(defn table
  "Render a table from input `opts`.

  `opts` should be a map with the following keys:

  + title
  + columns
  + sort-fn
  + rows
  + page-offset
  + page-size
  + show-header?
  + selected
  + show-filters?
  + filter-fns
  + filters
  + sort-column
  + sort-reverse?
  + sorters

  "
  [{:keys [id] :as instance-opts}]
  (let [instance-opts (cond-> instance-opts
                        (nil? id) (assoc :id (hash instance-opts)))
        selectfn      #(dispatch [:component-update-set [id :selected (hash %)]])]
    (fn [instance-opts]
      (let [cs          (subscribe [:get-components])
            state       (get @cs id)
            merged-opts (merge
                         table-defaults
                          ;; Set a default sort column if unset
                         {:sort-column (some-> instance-opts
                                               :columns
                                               (first)
                                               :path)}
                         instance-opts
                         state)

            {:keys [title columns sort-fn rows
                    page-offset page-size show-header? selected
                    show-filters? filter-fns filters
                    sort-column sort-reverse? sorters]
             :as   opts} merged-opts

            rowcount           (count rows)
            rows-processed     (cond->> rows
                                 filters      (apply-filters filters)
                                 sorters      (apply-sorters sorters)
                                 sort-column  (sort-by #(get-in % sort-column))
                                 sort-reverse? (reverse))
            rows-paginated     (if page-size
                                 (->> rows-processed
                                      (drop (* page-offset page-size))
                                      (take page-size)
                                      (map #(if (has? selected (hash %))
                                              (with-meta % {:selected true}) %)))
                                 rows-processed)
            rowcount-processed (count rows-processed)]
        [:div.table-meta
         (when title
           [:h2 title])
         (when (and (pos? (count filter-fns)) show-filters?)
           (table-filters filter-fns id))
         [:div.table
          (when show-header?
            (table-header opts))
          (table-rows opts rows-paginated selectfn)]
         (when (and page-size (>= rowcount page-size))
           [:div.pagination
            (table-pagination {:id                 id
                               :rowcount           rowcount
                               :rowcount-processed rowcount-processed
                               :offset             page-offset
                               :size               page-size})])]))))
