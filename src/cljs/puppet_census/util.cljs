(ns puppet-census.util
  (:require [clojure.pprint :refer [pprint]]))

(defn has? [ss s]
  (reduce #(or (= s %2) %1) false ss))

(defn seqf
  "Remove nils from `xs` and return `nil` if the sequence is empty."
  [xs]
  (seq (filter identity xs)))

(defn pretty-print
  [v]
  [:pre
   (with-out-str (pprint v))])

(defn to-json
  [ss]
  (.stringify js/JSON (clj->js ss)))

(defn to-map
  "Convert a vector of maps to a map of maps, using `path` as the key"
  [path]
  #(reduce (fn [acc x] (assoc acc (get-in x path) x)) {} %))

(defn to-map-kv
  "Convert a vector of maps to a map of maps, using `path` as the key"
  [path]
  #(reduce (fn [acc x] (assoc acc (keyword (get-in x path)) x)) {} %))
