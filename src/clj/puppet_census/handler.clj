(ns puppet-census.handler
  (:require
   [taoensso.timbre :as log]
   [config.core :refer [env]]

   ;; development
   [ring.util.response :refer [resource-response]]
   [ring.middleware.reload :refer [wrap-reload]]
   [ring.middleware.cors :refer [wrap-cors]]
   [shadow.http.push-state :as push-state]

   [reitit.ring :as ring]
   [reitit.http]
   [reitit.coercion.spec]
   [reitit.http.coercion :as coercion]
   [reitit.dev.pretty :as pretty]
   [reitit.interceptor.sieppari :as sieppari]
   [reitit.http.interceptors.parameters :as parameters]
   [reitit.http.interceptors.muuntaja :as muuntaja]
   [reitit.http.interceptors.exception :as exception]
   [reitit.http.interceptors.multipart :as multipart]
   ;; Uncomment to use
   [reitit.http.interceptors.dev :as dev]
   [reitit.http.spec :as spec]
   [spec-tools.spell :as spell]
   [ring.adapter.jetty :as jetty]
   [muuntaja.core :as m]
   [clojure.java.io :as io]
   [clojure.spec.alpha :as s]
   [spec-tools.core :as st]

   [clj-http.client :as http]

   [cheshire.core]

   [clojure.string :as str]))

(def notes-file
  (or (some-> (env :notes-file))
      "/etc/puppet-census/notes.edn"))

(defn puppetdb-request-handler
  "Forward a call to a resource to a backend API. NOTE: JSON only!"
  [{:keys [params uri request-method body headers query-string
           query-params] :as request}]

  ;; (:reitit.core/match
  ;;  :reitit.core/router
  ;;  :ssl-client-cert
  ;;  :protocol
  ;;  :remote-addr
  ;;  :params
  ;;  :headers
  ;;  :server-port
  ;;  :muuntaja/request
  ;;  :content-length
  ;;  :form-params
  ;;  :query-params
  ;;  :content-type
  ;;  :character-encoding
  ;;  :uri
  ;;  :server-name
  ;;  :query-string
  ;;  :path-params
  ;;  :muuntaja/response
  ;;  :body
  ;;  :scheme
  ;;  :request-method)

  (let [path (apply str (drop (count "/puppetdb") uri))
        resp (try
               (http/request
                {:url          (str (or (some-> (env :puppetdb))
                                        "http://localhost:62880") "/" path)
                 :method       request-method
                 :headers      (dissoc headers "content-length")
                 :query-params query-params
                 :body         (cheshire.core/encode (or (slurp body) nil))})
               (catch Exception ex
                 nil))]
    {:body    (:body resp)
     :status  (:status resp)
     :headers {"content-type"                 "application/json"
               "access-control-allow-headers" "content-type"
               "access-control-allow-origin"  "*"
               "access-control-allow-methods" (clojure.string/join "," ["get" "post"])}}))

(defn server-comment-handler
  []
  ())

(defn read-notes
  []
  (some->
   (try
     (slurp notes-file)
     (catch java.io.FileNotFoundException ex
       (spit notes-file {})
       {}))
   (clojure.edn/read-string)))

(def app
  (reitit.http/ring-handler
   (reitit.http/router
    [["/puppetdb/{*path}"
      {:handler puppetdb-request-handler}]
     ["/notes"
      {:get
       {:handler
        (fn [_args]
          {:body    (read-notes)
           :headers
           {"content-type"                 "application/json"
            "access-control-allow-origin"  "*"
            "access-control-allow-methods" (clojure.string/join "," ["get"])}})}}]

     ["/notes/{certname}"
      {:get
       {:handler
        (fn [{:keys [path-params] :as args}]
          {:body (get (read-notes) (:certname path-params) {})})}

       :options
       {:handler
        (fn [_args]
          {:headers {"access-control-allow-headers" "content-type"
                     "access-control-allow-origin"  "*"
                     "access-control-allow-methods" (clojure.string/join "," ["get", "post"])}})}
       :post
       {:handler
        (fn [{:keys [request-method path-params body body-params] :as args}]
          (let [certname   (:certname path-params)
                db         (read-notes)
                node-entry (get db certname)
                new-entry  (->> body-params
                                (merge node-entry)
                                (remove (fn [[_k v]] (nil? v)))
                                (into {}))
                new-db     (assoc db certname new-entry)]
            (spit notes-file new-db)
            {:body new-db
             :headers {"access-control-allow-origin"  "*"}}))}}]
     ["/" (ring/create-resource-handler)]
     ["/{*}" (ring/create-resource-handler)]]

    {;; :reitit.interceptor/transform dev/print-context-diffs
     ;; pretty context diffs

     :validate         spec/validate ;; enable spec validation for route data
     :reitit.spec/wrap spell/closed  ;; strict top-level validation
     :exception        pretty/exception
     :data             {:coercion     reitit.coercion.spec/coercion
                        :muuntaja     m/instance
                         ;; :middleware   [wrap-enforce-roles]
                        :interceptors [;; query-params & form-params
                                       (parameters/parameters-interceptor)
                                        ;; content-negotiation
                                       (muuntaja/format-negotiate-interceptor)
                                        ;; encoding response body
                                       (muuntaja/format-response-interceptor)
                                        ;; exception handling
                                       (exception/exception-interceptor)
                                        ;; decoding request body
                                       (muuntaja/format-request-interceptor)
                                        ;; coercing response bodys
                                       (coercion/coerce-response-interceptor)
                                        ;; coercing request parameters
                                       (coercion/coerce-request-interceptor)
                                        ;; multipart
                                       (multipart/multipart-interceptor)]}
      ;; required for catch-all covering all paths
     :conflicts        nil})
   (ring/redirect-trailing-slash-handler {:method :strip})
   {:executor sieppari/executor}))

(defn start []
  (println "server starting on port 3000")
  (jetty/run-jetty #'app {:port 3000, :join? false, :async true}))

(comment
  (def server
    (start))

  (.stop server))
