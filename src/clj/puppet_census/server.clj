(ns puppet-census.server
  (:require [puppet-census.handler :refer [app]]
            [config.core :refer [env]]
            [ring.adapter.jetty :refer [run-jetty]])
  (:gen-class))

(defn -main [& _args]
  (let [port (or (some-> (env :port)
                         (Integer.))
                 3000)]
    (run-jetty app {:port port :join? false})))
