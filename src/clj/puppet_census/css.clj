(ns puppet-census.css
  (:require [garden.def :refer [defstyles]]
            [garden.color :refer [darken lighten] :as color]
            [garden.def :refer [defstyles]]
            [garden.selectors
             :as g
             :refer [& nth-child first-child focus last-child
                     hover after before]]))

(def light-theme
  {:background :snow
   :text "#464344"})

(def light-gray "#f3f3f3")
(def dark-gray (darken light-gray 75))

(def background "#464344")
(def text       "#B9D0DF")
(def row        (lighten background 10))

(def state-colors
  {:.failed     :red
   :.pending    :orange
   :.changed    :blue
   :.unchanged  :green
   :.unreported :purple
   :.skipped    :yellow})

(def log-colors
  {:.err     :red
   :.notice  :lawngreen
   :.warning :orange})

(defstyles screen
  [:body
   {:margin 0}]
  [:#app
   [:&
    {:height      "100%"
     :font-family "Open Sans"}]
   [(g/> :.body)
    {:margin     "0.5em"
     :margin-top "4em"}]

   [:.divider
    {:margin              "2rem"
     :border-bottom-width "1px"
     :border-bottom-style :solid
     :border-bottom-color (lighten (color/from-name :gray) 30)}]

   [:.log
    (map (fn [[k c]]
           [(& k)
            {:background-color (lighten (color/from-name c) 30)}])
         log-colors)]

   [:.metrics
    [:&
     {:overflow :auto}]
    [:.metric
     [:&
      {:float  :left
       :margin "0 1em"
       :width  "10em"}

      (map (fn [[k c]]
             [(& k)
              [:h2
               {:color c}]])
           state-colors)]

     [:h2
      [:&
       {:margin "0.6em 0.2em 0"}]
      [:& :b
       {:font-size "140%"}]]

     [(g/> :div)
      {:margin "0 0.5em"}]]]

   [:.tag
    [(& :.button)
     [:&
      {:border-width  "1px"
       :border-style  :solid
       :border-radius "40%"
       :display       :inline-block
       :min-width     "1em"
       :text-align    :center
       :margin        "0 0.1em"
       :padding       "0 0.4em"}]
     (map (fn [[k c]]
            (let [lighter-color (darken (color/from-name c) 10)]
              [(& k)
               {:background-color lighter-color
                :border-color     lighter-color
                :color            (garden.color/invert (color/from-name c))}]))
          state-colors)]]

   [:nav
    [:&
     {:position         :absolute
      :top              0
      :width            "100%"
      :background-color (darken (color/from-name :gray) 10)}]

    [:a
     {:text-decoration :none}]

    [:ol
     {:margin  0
      :padding 0}]
    [:li
     [:&
      {:list-style :none
       :float      :left

       :position         :relative
       :border           "1px solid gray"
       :border-right     :none
       :background-color :inherit
       :color            :white}]
     [(& (last-child))
      {:border-right "1px solid gray"}]
     [(& (hover))
      {:background-color (color/from-name :gray)}]
     [(& (first-child))
      [:&
       {:border           0
        :background-color :inherit
        :color            :white}]
      [:& :*
       {:font-weight :bolder}]]
     [(& :.current)
      {:background-color (darken (color/from-name :gray) 2)}]

     [:div
      {:width "100%"}]]

    [:button
     {:border           0
      :padding          "1rem"
      :background-color :inherit
      :color            :white}]]]

  [:.specs
   [:.wrapper
    {:display   :flex
     :flex-flow "row wrap"
     :flex-grow 1}]
   [:.spec
    [:&
     {:display         :flex
      :flex            1
      :align-items     :center
      :justify-content :center
      :border-style    :solid
      :border-width    "1px"
      :border-color    (lighten (color/from-name :gray) 40)}]
    [:img
     {:width         "3rem"
      :padding-right "0.5em"}]]]

  [:span.state
   [:&
    [:&
     {:border-radius "50%"
      :width         "1rem"
      :height        "1rem"
      :display       :inline-block
      :margin-left   "0.5rem"}]

    (map (fn [[k c]]
           [(& k)
            {:background-color c}])
         state-colors)]]

  [:section
   [:h2
    {:margin-bottom 0}]
   [:.meta
    {:display     :inline-block
     :font-size   "70%"
     :margin-left "1em"}]
   [(g/+ :h2 :.meta)
    {:margin-bottom "0.5rem"
     :font-size     "85%"}]]

  [:#throbber
   [:&
    {:position :relative}]
   [(& :> :*)
    {:position :absolute
     :bottom   0
     :right    0}]]

  [:thead {:font-weight   "bold"
           :border-bottom {:width "1px"
                           :style "solid"
                           :color dark-gray}}]
  [:tbody
   [(& :> (nth-child 2)) {:background-color light-gray}]]
  [:tr:hover
   {:background-color (darken light-gray 10)}]
  [:.level1 {:color "green"}]

  [:td.tags
   {:max-width "10em"}]

  [:#throbber
   [:&
    {:position :relative}]
   [(& :> :*)
    {:position :absolute
     :bottom   0
     :right    0}]]

  [:.content {:padding "0 1em"}]

  [:.red
   {:background-color :red}]

  [:.table
   [:& {:display      "table"
        :margin       0
        :width        "100%"
        ;; :padding      "0.5rem"
        :border-width "0.1px"
        :border-style :solid
        :border-color (lighten (color/from-name :gray) 40)}]

   [:.tr
    [:&
     {:display "table-row"}]

    [(& (before))
     {:content :inherit}]

    [(& (last-child))
     [:.td
      {:border-bottom :none}]]

    [(& :.selected)
     {:background-color "#eee"}]

    [(& (hover))
     {:background-color (darken light-gray 10)}]

    [(& :.unchanged)
     {:background-color (lighten (color/from-name :lightgreen) 15)}]
    [(& :.changed)
     {:background-color (lighten (color/from-name :yellow) 42)}]
    [(& :.failed)
     {:background-color (lighten (color/from-name :red) 42)}]]

   [:.td
    [:pre
     {:margin 0}]
    [:&
     {:display             :table-cell
      :padding             "0.3em 0.5em"
      ;; :border-right "0.5px dashed gray"
      :border-bottom-width "0.1px"
      :border-bottom-style :solid
      :border-bottom-color (lighten (color/from-name :gray) 40)
      :min-width           "5rem"}]
    [(& :.sort)
     [(& (after))
      {:content "\" ↓\""}]

     [:&.reverse
      [(& (after))
       {:content "\" ↑\""}]]]]

   [:.thead
    [:& {:display     :table-header-group
         :font-weight :bold}]]
   [:.tbody
    [:& {:display :table-row-group}]]

   [:form
    [:textarea
     [:&
      {:background-color :inherit
       :resize           :none
       :border :none}]
     [(& (focus))
      {:background-color (lighten (color/from-name :yellow) 40)
       :resize           :both}]]]]

  [:.pagination
   [:.pages
    [:a [:& {:padding "0.2em"}

         [(& (first-child))
          {:padding-left 0}]

         [(& (last-child))
          {:padding-right 0}]

         [(& :.current) {:font-weight :bolder}]]]]]

  [:.refresh-buttons
   {:float :right}])
