(defproject puppet-census "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/clojurescript "1.10.773"
                  :exclusions [com.google.javascript/closure-compiler-unshaded
                               org.clojure/google-closure-library
                               org.clojure/google-closure-library-third-party]]
                 [thheller/shadow-cljs "2.11.4"
                  ;; Reduces uberjar size by some ~35MB
                  :exclusions [org.graalvm.js/js-scriptengine
                               org.graalvm.js/js]]
                 [reagent "0.10.0"]
                 [re-frame "1.1.1"]

                 [garden "1.3.10"]
                 [ns-tracker "0.4.0"]
                 [yogthos/config "1.1.7"]
                 [ring "1.8.1"]
                 [ring-cors "0.1.13"]
                 [clj-http "3.10.2"]
                 [metosin/reitit "0.5.5"
                  :exclusions [metosin/reitit-swagger
                               metosin/reitit-swagger-ui]]
                 [metosin/muuntaja "0.6.7"]
                 [metosin/sieppari "0.0.0-alpha13"]

                 [cljs-ajax "0.8.1"]
                 [day8.re-frame/http-fx "0.2.1"]

                 [com.taoensso/timbre "4.10.0"]

                 ;; l10n
                 [clojure-humanize "0.2.2"]]

  :plugins [[lein-shadow "0.2.0"]
            [lein-garden "0.3.0"]
            [lein-shell "0.5.0"]]

  :min-lein-version "2.9.0"

  :jvm-opts ["-Xmx1G"]

  :source-paths ["src/clj" "src/cljs"]

  :clean-targets ^{:protect false} ["resources/public/js/compiled" "target"
                                    "resources/public/css"]

  :main ^:skip-aot puppet-census.server

  :garden {:builds [{:id           "screen"
                     :source-paths ["src/clj"]
                     :stylesheet   puppet-census.css/screen
                     :compiler     {:output-to     "resources/public/css/screen.css"
                                    :pretty-print? true}}]}

  :shadow-cljs {:nrepl {:port 8777}

                :builds {:app {:target :browser
                               :output-dir "resources/public/js/compiled"
                               :asset-path "/js/compiled"
                               :modules {:app {:init-fn puppet-census.core/init
                                               :preloads [devtools.preload]}}

                               :devtools {:http-root "resources/public"
                                          :http-port 8280
                                          :http-handler puppet-census.handler/dev-handler}}}}

  :shell {:commands {"karma" {:windows         ["cmd" "/c" "karma"]
                              :default-command "karma"}
                     "open"  {:windows         ["cmd" "/c" "start"]
                              :macosx          "open"
                              :linux           "xdg-open"}}}

  :aliases {"dev"          ["do"
                            ["shell" "echo" "\"DEPRECATED: Please use lein watch instead.\""]
                            ["watch"]]
            "watch"        ["with-profile" "dev" "do"
                            ["shadow" "watch" "app" "browser-test" "karma-test"]]

            "prod"         ["do"
                            ["shell" "echo" "\"DEPRECATED: Please use lein release instead.\""]
                            ["release"]]

            "release"      ["with-profile" "prod" "do"
                            ["shadow" "release" "app"]]

            "build-report" ["with-profile" "prod" "do"
                            ["shadow" "run" "shadow.cljs.build-report" "app" "target/build-report.html"]
                            ["shell" "open" "target/build-report.html"]]

            "karma"        ["do"
                            ["shell" "echo" "\"DEPRECATED: Please use lein ci instead.\""]
                            ["ci"]]
            "ci"           ["with-profile" "prod" "do"
                            ["shadow" "compile" "karma-test"]
                            ["shell" "karma" "start" "--single-run" "--reporters" "junit,dots"]]}

  :profiles
  {:dev
   {:dependencies [[binaryage/devtools "1.0.2"]
                   [cider/cider-nrepl "0.25.3"]]
    :source-paths ["dev"]}

   :prod {}

   :uberjar {:source-paths ["env/prod/clj"]
             :omit-source  true
             :main         puppet-census.server
             :aot          [puppet-census.server]
             :uberjar-name "puppet-census.jar"
             :prep-tasks   ["compile" ["prod"] ["garden" "once"]]}

   :gitlab-ci {:local-repo "./.m2"}}

  :prep-tasks [["garden" "once"]]

  :repl
  {:plugins [[cider/cider-nrepl "0.25.3"]]})
