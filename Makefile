SOURCES := $(shell git ls-tree --name-only -r HEAD)
UBERJAR := ./target/puppet-census.jar

uberjar: $(UBERJAR)

$(UBERJAR): $(SOURCES)
	lein $(LEIN_ARGS) uberjar

.PHONY: docker
docker: $(UBERJAR) Dockerfile
	docker build --build-arg UBERJAR=$(UBERJAR) -t runejuhl/puppet-census:dev .

deps.tree deps-exclusions.tree: $(SOURCES)
	lein deps :tree > deps.tree 2> deps-exclusions.tree
