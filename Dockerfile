FROM openjdk:16-buster

ARG UBERJAR
ENV PORT=3000 \
    PUPPETDB=http://localhost:62880

ADD "${UBERJAR}" /usr/local/lib/census/puppet-census-uberjar.jar

ENTRYPOINT ["java", "-jar", "/usr/local/lib/census/puppet-census-uberjar.jar"]
